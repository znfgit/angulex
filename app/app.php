<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Debug\Debug;

require_once __DIR__.'/bootstrap.php';

/**
 * Enable Silex Error
 */
$app->error(function (\Exception $e, $code) use ($app){
    switch ($code) {
        case 404:
            $message = 'Halaman yang diminta tidak dapat ditampilkan.';
        	
            break;
        default:
            $message = 'Mohon Maaf. Ada kesalahan dalam sistem.';
    }

    $array = array(
        "message" => $message
    );

    return $app['twig']->render('err.twig', $array);
});

// default path
$app->get('/', function() use ($app){

    $array = array(
        'konektor' => "",
        'judul' => "Angulex [Angular-Silex Fusion]"
    );

    return $app['twig']->render('index.twig', $array);
});

$app->get('laman/{p}', function($p) use ($app){
    $array = array(
        'p' => $p,
        'konektor' => "../",
        'laman' => "",
        'tahun' => date('Y')
    );

    return $app['twig']->render('templates/'.$p.'.twig', $array);
});

return $app;

?>
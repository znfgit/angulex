var Angulex = angular.module('Angulex', [
  'ngRoute',
  'AngulexControllers'
]);

Angulex.config(['$routeProvider',
  	function($routeProvider) {
	    $routeProvider.
	    when('/', {
	        templateUrl: 'laman/beranda',
	        controller: 'berandaCtrl'
	    })
	    .
      when('/laman/dokumentasi', {
          templateUrl: 'laman/dokumentasi',
          controller: 'dokumentasiCtrl'
      })
      .
      when('/laman/tentang', {
          templateUrl: 'laman/tentang',
          controller: 'tentangCtrl'
      })
      .
	    otherwise({
	    	redirectTo: '/'
	    })
	    ;
	}
]);